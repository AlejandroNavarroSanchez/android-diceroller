package com.example.diceroller

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout

class MainActivity : AppCompatActivity() {
    private lateinit var rollButton: Button
    private lateinit var resetButton: Button
    private lateinit var dice1: ImageView
    private lateinit var dice2: ImageView
    private lateinit var layout: LinearLayout

    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Treure el títol
        supportActionBar?.hide()

        //Inicialitzem les variables. R(res).id(ids).roll_button(nom id variable)
        rollButton = findViewById(R.id.roll_button)
        resetButton = findViewById(R.id.reset_button)
        dice1 = findViewById(R.id.dice1)
        dice2 = findViewById(R.id.dice2)
        layout = findViewById(R.id.activity_layout)

        //Amaguem el botó de reset
        resetButton.visibility = View.INVISIBLE

        //Array de drawables
        val diceList = intArrayOf(R.drawable.dice_1, R.drawable.dice_2, R.drawable.dice_3,
            R.drawable.dice_4, R.drawable.dice_5, R.drawable.dice_6)
        var rand: Int = (0..5).random()

        //Establim el output del num pels daus
        rollButton.setOnClickListener {

            //Primer dau
            shakeImage(dice1)
            dice1.setImageDrawable(getDrawable(diceList[rand]))

            //Segon dau
            rand = (0..5).random()
            shakeImage(dice2)
            dice2.setImageDrawable(getDrawable(diceList[rand]))

            //Fer el botó de reset visible
            resetButton.visibility = View.VISIBLE
        }

        //Amagar daus quan cliques reset
        resetButton.setOnClickListener {
            dice1.setImageResource(R.drawable.empty_dice)
            dice2.setImageResource(R.drawable.empty_dice)
            //Amagar el botó de reset
            resetButton.visibility = View.INVISIBLE
        }

        dice1.setOnClickListener {
            rand = (0..5).random()
            shakeImage(dice1)
            dice1.setImageDrawable(getDrawable(diceList[rand]))
        }
        dice2.setOnClickListener {
            rand = (0..5).random()
            shakeImage(dice2)
            dice2.setImageDrawable(getDrawable(diceList[rand]))
        }

    }
    private fun shakeImage (image: ImageView) {
        image.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake))
    }
    /*private fun jackpot() {
        val jackpotBar = Snackbar.make(layout, "JACKPOT!", Snackbar.LENGTH_LONG)
        jackpotBar.show()
    }*/
}